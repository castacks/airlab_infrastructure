# Infrastructure

> Infrastructure, a deployment automation tool.

**Table Of Contents**

[TOC]

# Prerequisites

## System Requirements

**1. Linux System: Ubuntu 18.04**

   - Not tested on other versions.

**2. Install system packages without errors**

   - If you have errors and need to do `sudo apt-get install --fix-broken`, you must fix your system package install errors.
   - You will not be able to continue with the below readme until you have fixed all the broken packages.

**3. python2 (some operational tools do not work with python3 yet)**

        # installing python 2.7 and pip
        sudo apt update
        sudo apt install python2.7 python-pip

**4. ROS Melodic (optional)**

   - See the official [ROS install instructions](http://wiki.ros.org/melodic/Installation/Ubuntu).

**5. Azure Account**

  - Please notify your Azure maintainer for creating user accounts.
  - Go to the [portal.azure.com](https://portal.azure.com/#home) and verify you can login.

## SSH Keys

**Bitbucket SSH Keys**

- Generate ssh keys for infrastructure bitbucket:

        mkdir -p ~/.ssh/
        cd ~/.ssh/
        ssh-keygen

    - Answer the prompts from `ssh-keygen` as shown below:

            Enter file in which to save the key (/home/<USER-NAME>/.ssh/id_rsa): /home/<USER-NAME>/.ssh/bitbucket
            Enter passphrase (empty for no passphrase):

    - **DO NOT ENTER A PASSPHRASE on `ssh-keygen`! LEAVE IT BLANK.**
    - **Docker will not build successfully if you have a passphrase.**
    - Replace `<USER-NAME>` with your actual username

- Add the generated public key to your bitbucket user: see [**STEP 4**](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html#SetupanSSHkey-Step4.AddthepublickeytoyourBitbucketsettings)

- Add the ssh bitbucket key to your localhost ssh config file:

        # create (if not created) ssh config file
        touch ~/.ssh/config

        # open the ssh config file
        gedit ~/.ssh/config

        # Add the following to the top of the config file:
        IdentityFile ~/.ssh/bitbucket

        # exit the ssh config file

## Infrastructure Repository Install

**1. Install infrastructure dependencies**

        sudo apt-get update
        sudo apt install -y --no-install-recommends python python-setuptools python-pip
        pip2 install wheel --user
        pip2 install setuptools PyYAML pexpect --user

**2. Clone the infrastructure repo *(please follow these instructions exactly)* :**

        mkdir ~/infrastructure_ws/
        cd ~/infrastructure_ws/
        git clone git@bitbucket.org:castacks/airlab_infrastructure.git
        cd src
        ./install-infrastructure.bash --install

        # source your bashrc (or zhsrc, whichever shell you use)
        source ~/.bashrc

- Notify the maintainer if cloning or installing the infrastructure repository failed.

## Operational Tools Resources

Please have a basic understanding of the following the operational tools:

- [Git](https://git-scm.com/about)
- [Git Submodules](https://www.atlassian.com/git/tutorials/git-submodule)
- [ROS Melodic](http://wiki.ros.org/melodic)
- [Catkin](https://catkin-tools.readthedocs.io/en/latest/verbs/catkin_build.html)
- [Docker](https://docs.docker.com/get-started/)
- [Docker Compose (optional)](https://docs.docker.com/compose/)
- [Docker Context (optional)](https://docs.docker.com/engine/context/working-with-contexts/)
- [Azure (optional)](https://docs.microsoft.com/en-us/azure/?product=featured)
- [Azure CLI (optional)](https://docs.microsoft.com/en-us/cli/azure/?view=azure-cli-latest)
- [Terraform (optional)](https://www.terraform.io/)
- [Ansible (optional)](https://www.ansible.com/)
- [Tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/)

* * *

# Getting Started: Tutorials

The below instructions should get you started on a basic `infrastructure` setup locally or on Azure.

You will need to go through a few tutorials to have a working system.

### 1. Install Third-Party Tool (Required)

**Tutorial at:** [`docs/install-dependencies.md`](./docs/install-dependencies.md)

- Installs thirdparty tools (`docker, ansible, terraform,` etc.) required to setup deploy.

### 2. Azure Cloud Infrastructure Setup (Required)

**Tutorial at:** [`azurebooks/README.md`](https://bitbucket.org/castacks/azurebooks/src/master/)

- Follow this tutorial only if you are planning on using the Azure Cloud resource.

This tutorial will setup the following:

- Creates an example azure infrastructure VMs, virtual networking, VPN, etc.
- Sets up remote desktop access.

### 3. Ansible VM Setup (Required)

**Tutorial at:** [`robotbooks/README.md`](https://bitbucket.org/castacks/infrastructure_playbooks/src/master/)

- Follow this tutorial only if you are planning on using the Azure Cloud resource.

This tutorial will setup the following:

- Install all system packages to your VM.
- Setup VM OS configurations.

### 4. Docker Engine Setup & Catkin Workspace Build (Required)

**Tutorial at:** [`deployerbooks/README.md`](https://bitbucket.org/castacks/infrastructure_deployerbooks/src/master/)

This tutorial will setup the following:

- Assumes you have already setup the docker engine on the localhost.

- Install all docker images on the remote machines
    - *Docker images setup the all the submodules' package dependencies*

- Create all docker containers on the remote machines
    - *A docker container is the environment where you will be building & running the code*

- Builds catkin workspace inside the docker containers.

### 5. Managing Endpoints (Optional)

**Discussion at:** [`docs/discuss-managing-endpoints.md`](docs/discuss-managing-endpoints.md)

- Methods of managing multiple remote docker endpoints.
- Remote development workflow.

### 6. Continuous Integration (Optional)

This tutorial will setup the following:

- [Continuous Integration: Jenkins](https://bitbucket.org/castacks/ci_jenkins/wiki/Home)

### 7. Operational Tools (Optional)

**Discussion at:** [`docs/discuss-operation-tools.md`](docs/discuss-operation-tools.md)

- Operational tools used in deploy, their function and their general syntax.
- Helpful thidparty tools

### 8. More Tutorials (Optional)

Some tutorials are not found on the readme, but on the deploy "wiki" page:

- [Submodules: Getting Started With Basics](https://bitbucket.org/castacks/airlab_infrastructure/wiki/tutorials/submodules)

### 9. Common Questions and Issues (Optional)

**Discussion at:** [`docs/discuss-questions-issues.md`](docs/discuss-questions-issues.md)

- Common questions
- Known issues

* * *

# Who do I talk to

Please notify the maintainer(s) for any issues or questions:

- Katarina Cujic (`kcujic@andrew.cmu.edu`)

[[top]](#markdown-header-infrastructure)
