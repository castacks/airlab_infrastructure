# Install Requirements

## System ARM Container Provisioning Tools

Example ARM system would be NVIDIA Jetson Xavier.

### Install Docker

1. Install Docker

        sudo apt-get update
        sudo apt-get install curl
        curl -fsSL test.docker.com -o get-docker.sh && sh get-docker.sh

2. Add your user to the docker group

        sudo usermod -aG docker $USER

3. Reboot your system.

        sudo reboot -h now

4. Build an example docker image

        docker run hello-world

- Do not run `docker` with `sudo`...

### Docker Compose

There is [no offical](https://github.com/docker/compose/issues/6188) docker-compose binary for ARM, so docker-compose must be install using `python-pip`.

1. Install latest python & python pip packages

        sudo apt-get update
        sudo apt install -y --no-install-recommends python python-setuptools python-pip

2. Install docker-compose dependencies

        sudo apt install -y --no-install-recommends libssl-dev libffi-dev python-backports.ssl-match-hostname

3. Install docker-compose

        sudo pip install docker-compose

### Install NVIDIA Docker


Instead, `docker run` will pass the NVIDIA `/dev` devices to docker as arguments.
  - See [here](https://github.com/Technica-Corporation/Tegra-Docker) for more details.

To test out that docker NVIDIA passthu works, please try out the following:

1. Build deviceQuery on local system

        cd /usr/local/cuda/samples/1_Utilities/deviceQuery/
        make -j8
        ./deviceQuery

  * Make sure you **do not** see:

                cudaGetDeviceCount returned 35
                -> CUDA driver version is insufficient for CUDA runtime version
                Result = FAIL

2. Copy deviceQuery executable to the mmpug docker build context

        cp /usr/local/cuda/samples/1_Utilities/deviceQuery/deviceQuery ${MMPUG_PATH}/mmpug_docker/dockerfiles/cuda/arm/deviceQuery

3. Build the docker `deviceQuery` image

        docker-compose-wrapper --env arch=arm -f dockerfiles/cuda/cuda.yml build deviceQuery

4. Run the docker `deviceQuery` image

        docker-compose-wrapper --env arch=arm -f dockerfiles/cuda/cuda.yml up deviceQuery

