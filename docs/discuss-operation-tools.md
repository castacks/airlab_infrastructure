# Operational Tools Discussion

## About Operational Tools & Utilities

There are a few operational tools available to use:

`docker`

  - command interface to interact with `dockerfiles` found in `dockerbooks/[workspace name]/dockerfiles`

`docker-compose`

  - command interface to interact with `docker-compose.yml` files found in `dockerbooks/[workspace name]/dockerfiles`

`docker-compose-wrapper`

  - command interface to wrap `docker compose` and with `scenario` configuration files found in `dockerbooks/[workspace name]/scenarios`

`docker context`

  - command interface to manage multiple docker endpoints *(example: docker engine running on Azure VMs)*.

`deployer`

  - command interface to interact with `deployerfiles` found in `deployerbooks/`
  - automates running the tutorial steps, with realtime command output.

`ansible`

  - command interface to interact with `ansible playbooks` found in `robotbooks/`
  - automates installing dependencies and setting up systems.

`check-ssh-connect`

  - tests ssh connection to hosts listed in the local `~/.ssh/config`

  - **Example output**:

          # //////////////////////////////////////////////////////////////////////////////
          # == Testing SSH Connection ==
          # //////////////////////////////////////////////////////////////////////////////

          ugv1.ppc              FAIL
          ugv1.nuc              FAIL
          ugv1.xavier           FAIL
          azure.basestation     OK.
          azure.ugv1            OK.
          azure.ugv2            FAIL
          azure.ugv3            FAIL
          azure.uav1            OK.
          azure.uav2            FAIL
          azure.uav3            FAIL
          azure.uav4            FAIL
          azure.perception1     FAIL

`check-teamviewer-connect`

  - tests teamviewer connection to hosts listed in the local `~/.ssh/config`

  - **Example output**:

          # //////////////////////////////////////////////////////////////////////////////
          # == Testing SSH Connection ==
          # //////////////////////////////////////////////////////////////////////////////

          ugv1.ppc              FAIL
          ugv1.nuc              FAIL
          ugv1.xavier           FAIL
          azure.basestation     1713206669
          azure.ugv1            1714515085
          azure.ugv2            FAIL
          azure.ugv3            FAIL
          azure.uav1            1710017259
          azure.uav2            FAIL
          azure.uav3            FAIL
          azure.uav4            FAIL
          azure.perception1     FAIL

`deploy-cd`

  - changes current directory to the top level deploy src path.

`deploy-azure-limits-eastus`

  - shows vCPUs limits for *East US* region

  - **Example output**:

          Total Regional vCPUs  995  1000

`deploy-azure-limits-eastus2`

  - shows vCPUs limits for *East US 2* region

  - **Example output**:

          Total Regional vCPUs  31  350

`deploy-vpn-ca-cert`

  - outputs the `caCert` with the following command: `openssl x509 -in caCert.pem -outform der | base64 -w0 ; echo`

## Thirdparty Tools

**Some Helpful Tools For Remote Development**

- Improving the shell experience: `zsh`, `oh-my-zsh`
- Managing remote VM desktops: `rdp`, `teamviewer`
- Managing launch setups: `tmux`, `byobu`
- Remote desktop extensions on IDE, for example the [visual code plugin](https://code.visualstudio.com/docs/remote/remote-overview).
- Managing docker endpoints tools: `docker context`, `docker machine`, `docker swarm`
